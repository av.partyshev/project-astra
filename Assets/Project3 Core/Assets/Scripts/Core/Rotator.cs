﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Rotator : MonoBehaviour
{
    public Vector3 angularVelocity;

    private float direction_timer = 3f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (direction_timer < 0)
        {
            int i = Random.Range(0, 2);
            angularVelocity[i] = Random.Range(-0.5f, 0.5f);
            direction_timer = 1f;

        }

        transform.Rotate(angularVelocity);
        direction_timer -= Time.deltaTime;
    }
}
