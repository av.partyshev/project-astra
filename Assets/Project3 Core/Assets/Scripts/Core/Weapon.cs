﻿using Project3.Bullet;
using Project3.Bullet.Setup;
using UnityEngine.Events;

namespace Project3.Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Project3.Core;
    using UnityEngine;
    public abstract class Weapon:EnergyClient
    {
        [Serializable]
        public struct BulletDefault{
            public MeteoraSetup Meteora;
            public AsteroidSetup Asteroid;
            public HoundSetup Hound;
        }

        
        public LayerMask Bullets;

        // Start is called before the first frame update
        [Header("Clip Setup")]
        public int MaxClip;
        public int MinClip;
        public int Clip;
        public BulletDefault setup;
        protected List<GameObject> clip;
        [Header("Target")]
        public Transform target;

        public BulletClass bulletClass;
        
        
        public UnityEvent OnArm = new UnityEvent();
        public UnityEvent OnShoot = new UnityEvent();
        
        protected Vector3 targetPoint;
        
        
        protected float scrollInput;
        protected float scrollState;
        protected const string MouseScrollInput = "Mouse ScrollWheel";
        protected bool _generating = false;
        protected bool _generated = false;
        protected bool trigger;
        protected abstract IEnumerator Shoot();
        
        protected abstract  IEnumerator Reload();
        protected void Awake()
        {
            StartCoroutine(Reload());
            
            StartCoroutine(Shoot());
            clip=new List<GameObject>();
        }

        public virtual bool Damage(float amount)
        {

            return Use(amount);
            
        }

    }
}