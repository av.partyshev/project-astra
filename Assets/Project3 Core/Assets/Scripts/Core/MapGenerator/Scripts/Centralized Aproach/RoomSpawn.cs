﻿using System;
using UnityEngine;

namespace Project3.Core.MapGenerator.Scripts
{
    [Serializable]
    public struct JointLocations
    {
        public JointDirection direction;
        public Transform location;
        public float probability;
    }
    public class RoomSpawn : MonoBehaviour
    {
        public JointLocations[] JointSpring=new JointLocations[6];
        
        
        
        
    }
}