﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Project3.Core.MapGenerator.Scripts
{
    public class RoomSpawnManager : MonoBehaviour
    {
        public GameObject[,] layer;
        public int Vertical;
        public int Horizontal;

        private void Start()
        {
            layer=new GameObject[Horizontal,Vertical];
            
            
            
            
            
            
        }

        List<int[]> checkNeighbors(GameObject[,] map, int h, int v)
        {
            List<int[]> ret=new List<int[]>();
            int[] t;
            if (h + 1 < Horizontal)
            {


                if (map[h + 1, v] != null)
                {
                    t= new []{h + 1, v};
                    

                    ret.Add(t);
                }

                if (h>0)
                {

                    if (map[h - 1, v] != null)
                    {
                        t = new[] {h - 1, v};
                        ret.Add(t);
                    }
                }
            }

            if (v + 1 < Vertical)
            {
                if (map[h, v + 1] != null)
                {
                    t = new[] {h, v + 1};
                    ret.Add(t);
                }
                if (v>0)
                {
                    if (map[h, v - 1] != null)
                    {
                        t = new[] {h, v - 1};
                        ret.Add(t);
                    }
                }
            }




            return ret;
        }
        
        
        
    }
}