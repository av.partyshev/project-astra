﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Project3.Core.MapGenerator
{
    [Serializable]
    public struct BoundingPoints
    {
        public Transform up;
        public Transform down;
        public Transform south;
        public Transform north;
        public Transform west;
        public Transform east;
    }
    public class RoomBounds : MonoBehaviour
    {
        public Transform Center;
        public BoundingPoints bounds;

        public bool OverlapCheck()
        {

            Collider[] touches = Physics.OverlapBox(Center.position, new Vector3(bounds.east.localPosition.magnitude-0.1f*transform.localScale.x,
                bounds.up.localPosition.magnitude-0.1f*transform.localScale.y,
                bounds.north.localPosition.magnitude-0.1f*transform.localScale.z));
            
            if (touches.Length != 0)
            {
                DestroyImmediate(gameObject);
                return false;
            }

            Collider[] col = GetComponentsInChildren<Collider>();
            foreach (var c in col)
            {
                c.gameObject.layer = 0;
            }
            
            
            return true;
        }
        
        
    }
}