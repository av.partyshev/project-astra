﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using VolumetricLines;
using Random = UnityEngine.Random;

namespace Project3.Core.MapGenerator
{

    public enum JointDirection
    {
        NONE,
        UP,
        DOWN,
        NORTH,
        SOUTH,
        WEST,
        EAST

    }
    [Serializable]
    public struct GenerationPrefab
    {
        public GameObject gameObject;
        public float probabilityShare;
    }

    public class JointPoint : MonoBehaviour
    {
        public JointDirection Position;

        public bool Connected=false;
        public bool OpenToJoin=true;
        public List<GameObject> RoomPrefab;
        public float GenerationProbablility;
        public GameObject Seal;

        // Start is called before the first frame update
        private void Start()
        {
            if (Position == JointDirection.NONE)
            {
                OpenToJoin = false;
            }
            else
            {
                OpenToJoin = true;
            }
            //Seal.SetActive(true);
        }

        public void Update()
        {
            if (Input.GetKeyDown(KeyCode.Q))
            {
                print("Q");
                //print(Connected);
                //print(OpenToJoin);
                StartCoroutine(Generate());
            }
            
        }


        protected IEnumerator Generate()
        {
            
            int rand = Random.Range(0, 99);
            
            if (!Connected && OpenToJoin&&rand<GenerationProbablility)

            {
                GameObject Room = Instantiate(RoomPrefab[(int)(Random.Range(0f, (RoomPrefab.Count-1)*100f)/100f)]);
                RoomBounds bounds = Room.GetComponent<RoomBounds>();
                JointPoint point=null;
                Vector3 offset;
                switch (Position)
                {

                    case JointDirection.UP:
                        point = Room.GetComponentInChildren<DOWNPoint>();



                        break;
                    case JointDirection.DOWN:

                        point = Room.GetComponentInChildren<UPPoint>();


                        break;
                    case JointDirection.NORTH:

                        point = Room.GetComponentInChildren<SOUTHPoint>();


                        break;
                    case JointDirection.SOUTH:

                        point = Room.GetComponentInChildren<NORTHPoint>();


                        break;
                    case JointDirection.WEST:

                        point = Room.GetComponentInChildren<EASTPoint>();

                        yield return null;

                        break;
                    case JointDirection.EAST:

                        point = Room.GetComponentInChildren<WESTPoint>();



                        break;

                }

                offset = point.transform.localPosition;
                //print(offset);
                //print(transform.localPosition);
                offset.x *= Room.transform.localScale.x;
                offset.y *= Room.transform.localScale.y;
                offset.z *= Room.transform.localScale.z;
                Room.transform.position = transform.position-offset;
                if (bounds.OverlapCheck())
                {
                    point.Connected = true;
                    Connected = true;
                    OpenToJoin = false;
                    //Seal.SetActive(false);
                   //point.Seal.SetActive(false);
                }
                else
                {
                    OpenToJoin = false;
                    Connected = false;
                    //Seal.SetActive(true);
                }
                
                yield return null;
            }
            
            
            
        }
        

    }

        

}
