﻿namespace Project3.Core
{
    public class DamageRegistrator:EnergyClient
    {
        public virtual bool Damage(float amount)
        {
            m_EnergyContainer.Load();
            return Use(amount);
            
        }
        
        
    }
}