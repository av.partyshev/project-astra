﻿using System;
using System.Collections;
using UnityEngine;

namespace Project3.Core
{
    public class EnergyShield:EnergyContainer
    {
        public DualLayerBar EnergyBar;
        public GameObject NotEnoughEnergySign;
        public bool HideDisplay;
        
        private float consumption;

        protected override void Init()
        {
            StartCoroutine(EnableNotEnoughEnergySign());
            StartCoroutine(LoadTracker());
            
        }


        private void FixedUpdate()
        {
            consumption = SumUpConsumption();
            //print("Current Energy: "+CurrentEnergy);
//            print("Consume: "+consumption);
            
            CurrentEnergy = ActiveEnergy + BookedEnergy;
            if (CurrentEnergy<=MaxEnergy)
            {
                if(!RecentlyLoaded&&!Depleted)
                    ActiveEnergy += Regeneration * Time.fixedDeltaTime;
                //print("Regeneration" + Regeneration * Time.fixedDeltaTime);
            }
            else
            {
                ActiveEnergy = Mathf.Clamp(ActiveEnergy, 0, MaxEnergy - BookedEnergy);
            }
            
            
            if(CurrentEnergy-consumption<0||CurrentEnergy<0||ActiveEnergy-consumption<0)
            {
                NotEnoughEnergy = true;
                
                //Debug.LogWarning("Not Enough Energy");
                return;
            }
            
            //print("Plenty of Energy");
            
            ActiveEnergy -= consumption;

            if (HideDisplay) return;
            EnergyBar.ActiveValue = ActiveEnergy;
            EnergyBar.BookedValue = BookedEnergy;
            EnergyBar.TotalValue = CurrentEnergy;
            
        }

        private void Update()
        {
            
        }

        public IEnumerator EnableNotEnoughEnergySign()
        {

            while (isActiveAndEnabled)
            {

                yield return new WaitUntil(() => NotEnoughEnergy||Depleted);
                NotEnoughEnergySign.SetActive(NotEnoughEnergy||Depleted);
                yield return new WaitForSeconds(1);
                NotEnoughEnergy = false;
                NotEnoughEnergySign.SetActive(NotEnoughEnergy||Depleted);
                yield return null;


            }
            
            
            
            yield break;
        }
        

    }
}