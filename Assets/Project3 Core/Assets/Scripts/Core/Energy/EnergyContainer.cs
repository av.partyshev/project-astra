﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro.EditorUtilities;
using UnityEngine;
using UnityEngine.Events;

namespace Project3.Core
{
    public abstract class EnergyContainer : UnityEngine.MonoBehaviour
    {
        public float MaxEnergy;
        public float CurrentEnergy;
        public float BookedEnergy;
        public float ActiveEnergy;
        public float Regeneration;
        public float OverloadDelay;
        private bool underLoad;
        public List<EnergyClient> list;
        public bool NotEnoughEnergy;
        public UnityEvent ForcedEnergyOverloadEvent=new UnityEvent();// AKA Death
        public UnityEvent RebootEvent=new UnityEvent();// AKA Respawn
        public bool Depleted;
        public bool RecentlyLoaded;
        
        [Header("Live Update")] 
        public bool consumed;
        
        protected float SumUpConsumption()
        {
            float temp = 0;
            foreach (var client in list)
            {
                 temp+= client.consumption;
                 client.consumption = 0;
            }

            return temp;
        }

        public bool UpdateBooking()
        {

            Debug.LogWarning("Booking");
            float temp = 0;
            foreach (var client in list)
            {

                temp += client.booking;
            }

            if (ActiveEnergy - temp < 0)
            {
                NotEnoughEnergy = true;
            

                return false;
            }
            BookedEnergy = temp;
            ActiveEnergy -= BookedEnergy;
            return true;
        }
        private void Awake()
        {
            list = GetComponentsInChildren<EnergyClient>().ToList();
            BookedEnergy = 0;
            ActiveEnergy = MaxEnergy;
            CurrentEnergy = MaxEnergy;
            
            Init();   
        }

        protected abstract void Init();

        public virtual void ForcedEnergyOverloadCallback()
        {
            Depleted = true;
            BookedEnergy = 0;
            ActiveEnergy = 0;
            CurrentEnergy = 0;

            ForcedEnergyOverloadEvent?.Invoke();
            
        }

        public virtual void Reboot()
        {
            Depleted = false;
            list = GetComponentsInChildren<EnergyClient>().ToList();
            BookedEnergy = 0;
            ActiveEnergy = MaxEnergy;
            CurrentEnergy = MaxEnergy;
            RebootEvent?.Invoke();

        }

        public void Load()
        {
            underLoad = true;
            if (!RecentlyLoaded)
            {
                RecentlyLoaded = true;
            }
        }
        public IEnumerator LoadTracker()
        {

            while (isActiveAndEnabled)
            {

                yield return new WaitUntil(() => RecentlyLoaded);

                float start_timer=Time.time;
                float delta=0;
                while (delta<=OverloadDelay)
                {
                    if (underLoad)
                        delta = 0;
                    
                    yield return new WaitForFixedUpdate();
                    underLoad = false;
                    delta = Time.time - start_timer;

                }
                RecentlyLoaded = false;
                yield return null;


            }
            
            
            
            yield break;
        }
        
        
        
    }
}