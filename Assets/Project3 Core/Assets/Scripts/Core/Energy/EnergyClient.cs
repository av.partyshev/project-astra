﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace Project3.Core
{
    public abstract class EnergyClient : MonoBehaviour
    {
        public float consumption;
        public float booking;
        public bool booked;
        public bool use;
        public EnergyContainer m_EnergyContainer;
        
        

        protected void Start()
        {
            m_EnergyContainer = gameObject.GetComponentInParent<EnergyContainer>();
            
        }


        public bool Prep(float amount)
        {
            booked = true;
            booking = amount;
            return m_EnergyContainer.UpdateBooking();

        }

        public void UseBooked()
        {
            
            booked = false;
            //consumption += booking;
            booking = 0;
            use = true;
            m_EnergyContainer.UpdateBooking();

        }

        public bool Use(float amount)
        {
//            print("Quick Use");
            use = false;
            if (amount < 
                m_EnergyContainer.
                    ActiveEnergy)
            {
                use = true;
                consumption += amount;
            }
            else
            {
                Debug.LogWarning("Low Power");
                m_EnergyContainer.NotEnoughEnergy = true;
            }
            return use;
        }
        

        public void ForceEnergyDeprivation()
        {
            m_EnergyContainer.ForcedEnergyOverloadCallback();
        }
        
        
    }
}