﻿using System;
using Project3.Core;
using UnityEngine;

namespace Project3.Bullet
{
    public class ExplosionTrigger : MonoBehaviour
    {
        public Bullet source;
        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.tag == "Damagable")
            {
                DamageRegistrator energyClient = other.gameObject.GetComponent<DamageRegistrator>();
                
                print(source.damage);
                if (energyClient != null)
                {
                    print("hitting");
                    source.OnBulletHit(energyClient,source.damage);
                }
            }
        }
    }
}