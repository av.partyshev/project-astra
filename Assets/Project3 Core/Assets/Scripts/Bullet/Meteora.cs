﻿using System;
using System.Collections;
using System.Collections.Generic;
using Project3.Bullet.Setup;
using UnityEngine;

namespace Project3.Bullet
{
    public class Meteora:Bullet
    {
        
         public SphereCollider Explosion;
         public MeteoraSetup m_setup;
         private bool bloomed;
         public float BloomTime;
         public float BloomRadius;

         public float DamageScaleFactor;
         //public MeshRenderer ExplosiveEffect;
         public void Setup(MeteoraSetup stp)
         {
             m_setup = stp;
             BloomRadius = m_setup.BloomRadius;
             BloomTime = m_setup.BloomTime;
             DamageScaleFactor = m_setup.DamageScaleFactor;

         }
         private void Start()
         {

             
             Explosion.gameObject.SetActive(false);
         }

         void FixedUpdate()
        {
            // if((transform.position-parent.position).magnitude>1000)
            //     Destroy(gameObject);
            if (shot)
            {
                
            }

            if (shoot)
            {
                
                shot = true;
                shoot = false;
                //setVelocity = parent.TransformVector(Vector3.back).normalized * m_setup.ParticleVelocity;
                //r.velocity = setVelocity;
                StartCoroutine(Prep());
                StartCoroutine(Shoot());
            }


        }
        
        public override IEnumerator Prep()
        {
            
            Explosion.radius = 0.5f;
           /* innerRange = m_setup.ParticleRange * 2 / 3;
            yield return new WaitUntil(() => shot);
            Vector3 delta = transform.position - parent.position;
            
            do
            {
                if (delta.magnitude == 0)
                {
                    delta = parent.forward;
                }
                else
                    delta = transform.position - parent.position;

                setVelocity = delta.normalized * m_setup.ParticleVelocity;
                r.velocity = setVelocity;
                yield return null;
            } while (delta.magnitude < innerRange);

            setVelocity = delta.normalized * m_setup.ParticleVelocity / 2;
            r.velocity = setVelocity;
            do
            {
                curveAcceleration = m_setup.ParticleAcceleration * (target.position - transform.position).normalized;
                setVelocity += curveAcceleration * Time.deltaTime;

                r.velocity = setVelocity;
                yield return null;

            } while ((transform.position - parent.position).magnitude < m_setup.ParticleRange);



*/

            yield break;
        }
        
        

        public override IEnumerator Shoot()
        {
            damage = m_setup.EnergyPrice*m_setup.DamageScaleFactor;
            r = GetComponent<Rigidbody>();
            yield return new WaitUntil(() =>
                shot);
            r.velocity = (target.position-transform.position).normalized * m_setup.ShotVelocity;
            transform.SetParent(null);
            Explosion.gameObject.SetActive(true);
            transform.localScale=Vector3.one*m_setup.ShotSize;
            
            
            yield break;
        }


        public void BloomCallback()
        {
            if(bloomed)
                return;
            bloomed = true;
            StartCoroutine(Bloom());
        }

        public void DealExplosionDamage(Collider c)
        {
            
            
            
            
            
            
        }
        public IEnumerator Bloom()
        {
            float start_counter = Time.time ;
            float delta_t=0;
            // print();
            while (Explosion.gameObject.transform.lossyScale.x <=BloomRadius&& delta_t<BloomTime)
            {
                delta_t = Time.time - start_counter;
//                print(Explosion.gameObject.transform.lossyScale+"\n"+delta_t+"\n"+m_setup.BloomTime);
                // print();
                r.velocity=Vector3.zero;
                r.isKinematic = true;
                Explosion.gameObject.transform.SetParent(null);
                Explosion.gameObject.transform.localScale = Vector3.one*Mathf.Lerp(0, BloomRadius, delta_t/BloomTime);
                
                //transform.localScale = Vector3.one * Explosion.radius;
                yield return new WaitForFixedUpdate();
            }
            Explosion.transform.SetParent(transform);
            
            Destroy(Explosion.gameObject);
            Destroy(gameObject);
            yield break;
        }

        
    }
}