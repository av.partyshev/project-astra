﻿using System;
using System.Collections;
using System.Collections.Generic;
using Project3.Bullet.Setup;
using Project3.Core;
using Project3.Weapon.Caster;
using UnityEditor;
using UnityEngine;
using VolumetricLines;

namespace Project3.Bullet
{
    public class Asteroid : Bullet
    {
        private VolumetricLineBehavior trace;
        public AsteroidSetup setup;

        private void Start()
        {
            r = GetComponent<Rigidbody>();
            trace = GetComponent<VolumetricLineBehavior>();
            //
            // damage = 
            //     setup.EnergyPrice
            //     *DamageScaleFactor;
        }

        void FixedUpdate()
        {

            if (shot)
            {
                
            }

            if (shoot)
            {
                shot = true;
                shoot = false;
                
                damage = setup.EnergyPrice*setup.DamageScaleFactor;
                //setVelocity = parent.TransformVector(Vector3.back).normalized * setup.ParticleVelocity;
                //r.velocity = setVelocity;
                StartCoroutine(Prep());
                StartCoroutine(Shoot());
            }


        }

        public override IEnumerator Shoot()
        {
            yield return new WaitUntil(() =>
                shot && (transform.position - parent.position).magnitude > setup.ParticleRange);
            trace = gameObject.GetComponent<VolumetricLineBehavior>();
            trace.enabled = true;
            r.velocity = Vector3.zero;
            trace.LineColor = setup.RayColor;
            trace.TemplateMaterial = setup.Trace;

            trace.LineWidth = setup.RayThickness;
            trace.LightSaberFactor = setup.RayGlowFactor;
            trace.StartPos = Vector3.zero;
            Vector3 offset = transform.position - parent.position;
            trace.EndPos = transform.InverseTransformPoint(target.position + offset / 10);
            float delta = setup.RayThickness / (setup.BeamTime / Time.deltaTime);
            float timer = Time.time;
            transform.SetParent(null);
            while (Time.time - timer < setup.BeamTime)
            {
                trace.LineWidth -= delta;
                yield return null;
            }
            RaycastHit hit;
            Vector3 fromPosition = transform.position;
            Vector3 toPosition = target.position;
            Vector3 direction = toPosition - fromPosition;
         
     
            if(Physics.Raycast(transform.position,direction,out hit))
            {
                if (hit.collider.gameObject.tag == "Damagable")
                {

                    DamageRegistrator energyClient = hit.collider.gameObject.GetComponent<DamageRegistrator>();
                    if(energyClient!=null)
                        OnBulletHit(energyClient,damage);
                }
            }

            
            
            
            
            
            
            //Debug.LogError("Paused");
            Destroy(gameObject);



            yield break;
        }

        public override IEnumerator Prep()
        {
            innerRange = setup.ParticleRange * 2 / 3;
            yield return new WaitUntil(() => shot);

            Vector3 delta = transform.position - parent.position;

            do
            {
                if (delta.magnitude == 0)
                {
                    delta = parent.forward;
                }
                else
                    delta = (transform.position - parent.position)+Vector3.up*0.20f;

                setVelocity = delta.normalized * setup.ParticleVelocity;
                r.velocity = setVelocity;
                yield return null;
            } while (delta.magnitude < innerRange);

            setVelocity = delta.normalized * setup.ParticleVelocity / 2;
            r.velocity = setVelocity;
            do
            {
                curveAcceleration = setup.ParticleAcceleration * (target.position - transform.position).normalized;
                setVelocity += curveAcceleration * Time.deltaTime;

                r.velocity = setVelocity;
                yield return null;

            } while ((transform.position - parent.position).magnitude < setup.ParticleRange);





            yield break;
        }
    }
}
