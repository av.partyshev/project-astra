﻿using System.Collections;
using System.Collections.Generic;
using Project3.Core;
using UnityEditor.PackageManager;
using UnityEngine;

namespace Project3.Bullet

{
    public class Bullet : MonoBehaviour
    {
        protected Rigidbody r;
        protected bool shot;
        public bool shoot;
        public float damage;
        public Vector3 curveAcceleration = new Vector3();
        protected Vector3 setVelocity;
        public Transform target;
        public Transform parent;
        protected float innerRange;
        
        public virtual IEnumerator  Shoot()
        {
            yield break;
        }
        public virtual IEnumerator Prep()
        {
            yield break;
        }

        public virtual void OnBulletHit(DamageRegistrator client,float dmg)
        {
            //print(dmg);
            bool b = client.Damage(dmg);
//            print(b);
            if (!b)
            {
                
                client.ForceEnergyDeprivation();
            };
        }

        public virtual void OnBulletHit(DamageRegistrator client)
        {
            OnBulletHit(client, damage);
        }
    }
}