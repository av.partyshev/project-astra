﻿using System;
using UnityEngine;

namespace Project3.Bullet.Setup
{
    [Serializable]
    public class AsteroidSetup:BulletSetup
    {
        
        
        [Header("Simultaneous Shots")]
        public int MinSimultaneousShots;
        public int MaxSimultaneousShots;
        public int SimultaneousShots;
        [Header("Shot Line")]
        public Material Trace;
        public Color RayColor;
        public float RayThickness;
        public float RayGlowFactor;
        public float BeamTime;
        
    }
}