﻿using System;
using UnityEngine;

namespace Project3.Bullet.Setup
{
    [Serializable]
    public class HoundSetup
    {
        [Header("Shot Phase")] 
        public float Tracking;
        
    }
}