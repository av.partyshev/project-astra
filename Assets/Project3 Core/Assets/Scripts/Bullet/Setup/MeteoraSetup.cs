﻿using System;
using UnityEngine;

namespace Project3.Bullet.Setup
{
    [Serializable]
    public class MeteoraSetup:BulletSetup
    {
        
        
        [Header("Shot Phase")] 
        public float MinShotSize;
        public float MaxShotSize;
        public float ShotSize;
        
        public float MinShotVelocity;
        public float MaxShotVelocity;
        public float ShotVelocity;
        [Header("Blossom Phase")] 
        public float MinBloomRadius;
        public float MaxBloomRadius;
        public float BloomRadius;
        
        public float MinBloomTime;
        public float MaxBloomTime;
        public float BloomTime;
        // public float 
    }
}