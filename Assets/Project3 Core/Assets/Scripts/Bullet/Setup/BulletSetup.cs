﻿using System;
using UnityEngine;

namespace Project3.Bullet.Setup
{
    [Serializable]
    public class BulletSetup
    {
        
        public GameObject Prefab;
        public float ShotRange;
        [Header("RPM Setup")] 
        public float MinRPM;
        public float MaxRPM;
        public float RPM;
        [Header("Damage")] 
        public float DamageScaleFactor;
        public float MinDamageScaleFactor;
        public float MaxDamageScaleFactor;
        [Header("Particle Ready")]
        public float ParticleRange;
        public float ParticleVelocity;
        public float ParticleAcceleration;
        [Header("Energy")]
        public float EnergyPrice;
        public float MinEnergyPrice;
        public float MaxEnergyPrice;
        
    }
}