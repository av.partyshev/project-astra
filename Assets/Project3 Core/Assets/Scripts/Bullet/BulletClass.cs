﻿using Project3.Bullet.Setup;

namespace Project3.Bullet
{
    public enum BulletClass
    {
        Meteora,
        Asteroid,
        Hound
    }
}