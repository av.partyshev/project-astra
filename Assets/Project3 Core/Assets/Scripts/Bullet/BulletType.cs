﻿namespace Project3.Bullet
{
    public enum BulletType
    {
        Kinetic,
        Energy
    }
}