using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using KinematicCharacterController;
using System;
using Project3.Core;
using TMPro;

namespace Project3.ControllerTesting
{
    public struct PlayerCharacterInputs
    {
        public float MoveAxisForward;
        public float MoveAxisRight;
        public Quaternion CameraRotation;
        public bool JumpDown;
        public bool AlternateMobility;
        public bool Run;
        public bool AimDown;
    }

    public enum MobilityType
    {
        TrippleJump=0,
        FadeJump=1,
        Levitate=2,
        BurstGlide=3,
        BlinkReverse=4,
        Hook=5
    }

    public class Project3CharacterController : EnergyClient, ICharacterController
    {
        public KinematicCharacterMotor Motor;

        [Header("Stable Movement")]
        public float MaxStableMoveSpeed = 10f;
        public float MaxStableRunSpeed = 15f;
        public float StableMovementSharpness = 15;
        public float OrientationSharpness = 10;

        [Header("Air Movement")]
        public float MaxAirMoveSpeed = 10f;
        public float MaxAirMoveSpeedBonus = 2f;
        public float AirAccelerationSpeed = 5f;
        public float Drag = 0.1f;

        [Header("Mobility")] 
        public MobilityType CharaterMobility;
        public bool AllowJumpingWhenSliding = false;
        public bool AllowMobility = false;
        public bool AllowWallJump = false;
        private float _jumpSpeed = 10f;
        public float JumpSpeed = 13f;
        public float MobilityStat = 20f;
        public float JumpPreGroundingGraceTime = 0f;
        public float JumpPostGroundingGraceTime = 0f;
        [SerializeField, ReadOnly]
        
        private int airJumpCounter = 0;

        [Header("Misc")]
        public Vector3 Gravity = new Vector3(0, -30f, 0);
        public Transform MeshRoot;


        private Vector3 _moveInputVector;
        private Vector3 _lookInputVector;
        private bool _run = false;
        private bool _jumpRequested = false;
        
        [SerializeField, ReadOnly]
        private bool _mobilityRequested = false;
        private bool _jumpConsumed = false;
        private bool _jumpedThisFrame = false;
        private float _timeSinceJumpRequested = Mathf.Infinity;
        private float _timeSinceLastAbleToJump = 0f;
        private bool _doubleJumpConsumed = false;
        private bool _canWallJump = false;
        private Vector3 _wallJumpNormal;



        #region Mobility Speciffic Variables

        
        
        [Header("Tripple Jump Stats")]
        public int MaxAirJumps = 0;

        
        
        [Header("FadeJump Stats")] 
        [Range(0f,1f)]
        public float FadeRate=0.1f;

        
        [SerializeField, ReadOnly]
        private float _currentMobilityStat=0;
        


        [Header("Levitation Stats")] 
        public float MaxLevitationTime = 1.75f;
        public float VerticalLevitateAcceleration = 2f;
        public float BreakFallVelocity = 10f;
        
        [SerializeField, ReadOnly]
        private bool _mobilityToggle=false;
        [SerializeField, ReadOnly]
        private float _mobilityTimer=0f;

        [Header("Burst Glide Stats")] 
        
        [SerializeField, ReadOnly]
        private bool _initialBurst=false;


        [Header("Blink-Reverse Stats")] 
        
        
        public float MaxReverseTimer = 1.75f;
        
        [SerializeField, ReadOnly]
        private float _reverseTimer = 0;
        [SerializeField, ReadOnly]
        private bool _initialDash=false;
        [SerializeField, ReadOnly]
        private bool _blinkToggle=false;
        [Header("Energy for Jumps")]
        public float TrippleJumpEnergy;
        public float FadeJumpEnergy;
        public float LevitationEnergyRate;
        public float BurstGlideEnergyBurst;
        public float BlinkEnergy;
        public float ReverseEnergy;
        
        public TMP_Text JumpStatState;

        void updateJumpStateOut()
        {

            switch (CharaterMobility)
            {
                case MobilityType.TrippleJump:
                    JumpStatState.text = (MaxAirJumps - airJumpCounter).ToString();
                    
                    break;
                case MobilityType.FadeJump:
                    
                    JumpStatState.text = String.Format("{0:0.00}",Math.Round(_currentMobilityStat,2).ToString());
                    break;
                case MobilityType.Levitate:
                    
                    JumpStatState.text = String.Format("{0:0.00}",Math.Round(MaxLevitationTime-_mobilityTimer,2).ToString());
                    break;
                case MobilityType.BurstGlide:
                    
                    JumpStatState.text = String.Format("{0:0.00}",Math.Round(MaxLevitationTime*0.75f-_mobilityTimer,2).ToString());
                    break;
                case MobilityType.BlinkReverse:
                    JumpStatState.text = String.Format("{0:0.00}",Math.Round(MaxReverseTimer-_reverseTimer,2).ToString());

                    break;
                case MobilityType.Hook:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
        }
        [Serializable]
        struct CharState
        {
            public Vector3 position;
            public Quaternion rotation;
            public Vector3 velocity;
        }
        
        private CharState _reverseState;
        
        #endregion

        private Vector3 currentVelocityTemp=new Vector3();

        public void ChangeMobMode(int index)
        {
            switch (index)
            {
                case 0 :
                    CharaterMobility = MobilityType.TrippleJump;
                    break;
                case 1 :
                    CharaterMobility = MobilityType.FadeJump;
                    break;
                case 2 :
                    CharaterMobility = MobilityType.Levitate;
                    break;
                case 3 :
                    CharaterMobility = MobilityType.BurstGlide;
                    break;
                case 4 :
                    CharaterMobility = MobilityType.BlinkReverse;
                    break;
                default:
                    break;
            }
        }
        private void Start()
        {
            // Assign to motor
            Motor.CharacterController = this;
        }

        /// <summary>
        /// This is called every frame by MyPlayer in order to tell the character what its inputs are
        /// </summary>
        public void SetInputs(ref PlayerCharacterInputs inputs)
        {
            _jumpSpeed = JumpSpeed;
            MaxStableRunSpeed = MobilityStat;
            // Clamp input
            Vector3 moveInputVector =
                Vector3.ClampMagnitude(new Vector3(inputs.MoveAxisRight, 0f, inputs.MoveAxisForward), 1f);

            // Calculate camera direction and rotation on the character plane
            Vector3 cameraPlanarDirection =
                Vector3.ProjectOnPlane(inputs.CameraRotation * Vector3.forward, Motor.CharacterUp).normalized;
            if (cameraPlanarDirection.sqrMagnitude == 0f)
            {
                cameraPlanarDirection = Vector3.ProjectOnPlane(inputs.CameraRotation * Vector3.up, Motor.CharacterUp)
                    .normalized;
            }

            Quaternion cameraPlanarRotation = Quaternion.LookRotation(cameraPlanarDirection, Motor.CharacterUp);

            // Move and look inputs
            _moveInputVector = cameraPlanarRotation * moveInputVector;
            _lookInputVector = cameraPlanarDirection;
            if (Motor.GroundingStatus.IsStableOnGround)
            {
                _run = inputs.Run;
                _mobilityToggle = false;
                _mobilityRequested = false;
                _mobilityTimer = 0f;
                _initialBurst = false;
            }

            if (_reverseTimer > MaxReverseTimer)
            {
                _reverseState=new CharState();
                
                _reverseTimer = 0;
                _blinkToggle = false;
                _initialDash = false;
            }
            // Jumping input
            if (inputs.JumpDown)
            {
                _timeSinceJumpRequested = 0f;
                if (Motor.GroundingStatus.IsStableOnGround)
                {
                    _jumpRequested = true;
                }
                else
                {
                    switch (CharaterMobility)
                    {
                        case MobilityType.TrippleJump:
                            _mobilityRequested = true;
                            break;
                        case MobilityType.FadeJump:
                            _mobilityRequested = true;
                            break;
                        case MobilityType.Levitate:
                            _mobilityToggle = !_mobilityToggle;
                            break;
                        case MobilityType.BurstGlide:
                            
                            _mobilityToggle = !_mobilityToggle;
                            
                            break;
                        default:
                            break;
                    }
                    
                }
            }

            if (inputs.AlternateMobility)
            {
                switch (CharaterMobility)
                    {
                        case MobilityType.BlinkReverse:
                            if (!_blinkToggle)
                            {
                                if (Use(BlinkEnergy))
                                {
                                    _blinkToggle = true;
                                }
                            }
                            else
                            {
                                if (Use(ReverseEnergy))
                                {
                                    _blinkToggle = false;
                                }
                                
                            }
                            break;
                        case MobilityType.Hook:
                            
                            _mobilityToggle = !_mobilityToggle;
                            
                                break;
                        default:
                            break;
                    }
            }
        }
        public void SetMobilityStat(String s)
        {
            MobilityStat = float.Parse(s);
        }
        /// <summary>
        /// (Called by KinematicCharacterMotor during its update cycle)
        /// This is called before the character begins its movement update
        /// </summary>
        /// 
        public void BeforeCharacterUpdate(float deltaTime)
        {
            updateJumpStateOut();
        }

        /// <summary>
        /// (Called by KinematicCharacterMotor during its update cycle)
        /// This is where you tell your character what its rotation should be right now. 
        /// This is the ONLY place where you should set the character's rotation
        /// </summary>
        public void UpdateRotation(ref Quaternion currentRotation, float deltaTime)
        {
            if (_lookInputVector != Vector3.zero && OrientationSharpness > 0f)
            {
                // Smoothly interpolate from current to target look direction
                Vector3 smoothedLookInputDirection = Vector3.Slerp(Motor.CharacterForward, _lookInputVector, 1 - Mathf.Exp(-OrientationSharpness * deltaTime)).normalized;

                // Set the current rotation (which will be used by the KinematicCharacterMotor)
                currentRotation = Quaternion.LookRotation(smoothedLookInputDirection, Motor.CharacterUp);
            }
        }

        /// <summary>
        /// (Called by KinematicCharacterMotor during its update cycle)
        /// This is where you tell your character what its velocity should be right now. 
        /// This is the ONLY place where you can set the character's velocity
        /// </summary>
        public void UpdateVelocity(ref Vector3 currentVelocity, float deltaTime)
        {
            Vector3 targetMovementVelocity = Vector3.zero;
            MaxAirMoveSpeed = (_run ? MaxStableRunSpeed : MaxStableMoveSpeed)+MaxAirMoveSpeedBonus;
            MaxLevitationTime = MobilityStat / 10;
            currentVelocityTemp = currentVelocity;
            if (Motor.GroundingStatus.IsStableOnGround)
            {
                // Reorient velocity on slope
                currentVelocity = Motor.GetDirectionTangentToSurface(currentVelocity, Motor.GroundingStatus.GroundNormal) * currentVelocity.magnitude;

                // Calculate target velocity
                Vector3 inputRight = Vector3.Cross(_moveInputVector, Motor.CharacterUp);
                Vector3 reorientedInput = Vector3.Cross(Motor.GroundingStatus.GroundNormal, inputRight).normalized * _moveInputVector.magnitude;
                targetMovementVelocity = reorientedInput * (_run?MaxStableRunSpeed:MaxStableMoveSpeed);

                // Smooth movement Velocity
                currentVelocity = Vector3.Lerp(currentVelocity, targetMovementVelocity, 1 - Mathf.Exp(-StableMovementSharpness * deltaTime));
            }
            else
            {
                // Add move input
                if (_moveInputVector.sqrMagnitude > 0f)
                {
                    targetMovementVelocity = _moveInputVector * MaxAirMoveSpeed;

                    // Prevent climbing on un-stable slopes with air movement
                    if (Motor.GroundingStatus.FoundAnyGround)
                    {
                        Vector3 perpenticularObstructionNormal = Vector3.Cross(Vector3.Cross(Motor.CharacterUp, Motor.GroundingStatus.GroundNormal), Motor.CharacterUp).normalized;
                        targetMovementVelocity = Vector3.ProjectOnPlane(targetMovementVelocity, perpenticularObstructionNormal);
                    }

                    Vector3 velocityDiff = Vector3.ProjectOnPlane(targetMovementVelocity - currentVelocity, Gravity);
                    currentVelocity += velocityDiff * AirAccelerationSpeed * deltaTime;
                }

                // Gravity
                currentVelocity += Gravity * deltaTime;

                // Drag
                currentVelocity *= (1f / (1f + (Drag * deltaTime)));
                
            }

            // Handle jumping
            
            _jumpedThisFrame = false;
            _timeSinceJumpRequested += deltaTime;
            
            
            // Handle Mobility
            Vector3 inputDirection = targetMovementVelocity.normalized;
            Vector3 movementDirection = currentVelocity.normalized;
            //print(movementDirection);
            //print(currentVelocity.y);
            if (CharaterMobility == MobilityType.BlinkReverse)
                _jumpSpeed = JumpSpeed + MobilityStat * 0.5f;
            else
            {
                _jumpSpeed = JumpSpeed;
            }
            if (AllowMobility)
            {
                
                switch (CharaterMobility)
                {
                        
                    case MobilityType.TrippleJump:
                        if ((airJumpCounter<MaxAirJumps)&&_mobilityRequested && (AllowJumpingWhenSliding ? !Motor.GroundingStatus.FoundAnyGround : !Motor.GroundingStatus.IsStableOnGround))
                        {
                            if (Use(TrippleJumpEnergy))
                            {
                                Motor.ForceUnground(0.1f);
                                //print(currentVelocity);
                                // Add to the return velocity and reset jump state
                                currentVelocity +=
                                    ((Motor.CharacterUp * 1.15f + inputDirection * 0.5f) * MobilityStat) -
                                    Vector3.Project(currentVelocity, Motor.CharacterUp);
                                _mobilityRequested = false;
                                airJumpCounter++;
                                _jumpedThisFrame = true;
                            }

                        }
                        break;
                    case MobilityType.FadeJump:
                        if (_currentMobilityStat>=0.1f &&_mobilityRequested&& (AllowJumpingWhenSliding ? !Motor.GroundingStatus.FoundAnyGround : !Motor.GroundingStatus.IsStableOnGround))
                        {
                            if (Use(FadeJumpEnergy))
                            {


                                Motor.ForceUnground(0.1f);
                                //print(currentVelocity);
                                // Add to the return velocity and reset jump state
                                currentVelocity += ((Motor.CharacterUp) * _currentMobilityStat) +
                                                   inputDirection * MobilityStat -
                                                   Vector3.Project(currentVelocity, Motor.CharacterUp);
                                _mobilityRequested = false;
                                _currentMobilityStat -= FadeRate * MobilityStat;
                                _jumpedThisFrame = true;
                            }
                        }
                        break;
                    case MobilityType.Levitate:
                        if (_mobilityTimer<MaxLevitationTime&&_mobilityToggle && (AllowJumpingWhenSliding ? !Motor.GroundingStatus.FoundAnyGround : !Motor.GroundingStatus.IsStableOnGround))
                        {
                            if (Use(LevitationEnergyRate * Time.fixedDeltaTime))
                            {

                                _mobilityTimer += deltaTime;
                                Motor.ForceUnground(0.1f);
                                // Add to the return velocity and reset jump state
                                //currentVelocity += ((Motor.CharacterUp+jumpBooster) * _currentMobilityStat) - Vector3.Project(currentVelocity, Motor.CharacterUp);
                                // print(currentVelocity.y);
                                if (currentVelocity.y < -BreakFallVelocity)
                                    currentVelocity.y = 0;
                                currentVelocity += (-Gravity + Motor.CharacterUp * VerticalLevitateAcceleration) *
                                                   deltaTime;
                                //_mobilityRequested = false;
                                //_currentMobilityStat-=FadeRate;

                                _jumpedThisFrame = true;
                            }
                            else
                            {
                                _mobilityToggle = false;
                            }
                        }

                        break;
                    case MobilityType.BurstGlide:
                        if (_mobilityTimer < MaxLevitationTime*0.75f && _mobilityToggle && (AllowJumpingWhenSliding
                            ? !Motor.GroundingStatus.FoundAnyGround
                            : !Motor.GroundingStatus.IsStableOnGround))
                        {
                            if (Use(BurstGlideEnergyBurst * Time.fixedDeltaTime))
                            {


                                Motor.ForceUnground(0.1f);
                                if (!_initialBurst)
                                {
                                    // Add to the return velocity and reset jump state
                                    currentVelocity += ((Motor.CharacterUp) * _currentMobilityStat) -
                                                       Vector3.Project(currentVelocity, Motor.CharacterUp);
                                    _initialBurst = true;
                                    _mobilityTimer += deltaTime;

                                    currentVelocity +=
                                        (-Gravity + Motor.CharacterUp * VerticalLevitateAcceleration * 0.75f) *
                                        deltaTime;
                                }
                                else
                                {
                                    _mobilityTimer += deltaTime;
                                    currentVelocity += ((Motor.CharacterUp) * _currentMobilityStat * 0.5f) -
                                                       Vector3.Project(currentVelocity, Motor.CharacterUp);
                                    currentVelocity += (-Gravity + Motor.CharacterUp * VerticalLevitateAcceleration) *
                                                       deltaTime;
                                }

                                
                            }
                            else
                            {
                                _mobilityToggle = false;
                            }
                            

                            _jumpedThisFrame = true;
                        }

                        break;
                    case MobilityType.BlinkReverse:
                        if (_reverseTimer < MaxReverseTimer&& _blinkToggle)
                        {
                            
                            
                            if (!_initialDash)
                            {
                                
                                Motor.ForceUnground(0.1f);
                                // Add to the return velocity and reset jump state
                                _reverseState.position = Motor.InitialSimulationPosition;
                                _reverseState.rotation = Motor.InitialSimulationRotation;
                                _reverseState.velocity = currentVelocity;
                                currentVelocity += ((inputDirection) * _currentMobilityStat * 7f) -
                                                   Vector3.Project(currentVelocity, Motor.CharacterUp);
                                _initialDash = true;
                                
                            }
                            
                            _reverseTimer += deltaTime;

                        }else if (_reverseTimer < MaxReverseTimer &&_reverseTimer > 0.01f&& !_blinkToggle)
                        {
                            
                            Motor.SetPosition(_reverseState.position);
                            currentVelocity = _reverseState.velocity;
                            Motor.SetRotation(_reverseState.rotation);
                            _reverseTimer = 0;
                            _blinkToggle = false;
                            _initialDash = false;
                            

                        }

                        
                        break;
                    case MobilityType.Hook:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            
            if (_jumpRequested)
            {
                // See if we actually are allowed to jump
                if (_canWallJump ||
                    (!_jumpConsumed && ((AllowJumpingWhenSliding ? Motor.GroundingStatus.FoundAnyGround : Motor.GroundingStatus.IsStableOnGround) || _timeSinceLastAbleToJump <= JumpPostGroundingGraceTime)))
                {
                    // Calculate jump direction before ungrounding
                    Vector3 jumpDirection = Motor.CharacterUp;
                    if (_canWallJump)
                    {
                        jumpDirection = _wallJumpNormal+Vector3.up*2;
                    }
                    else if (Motor.GroundingStatus.FoundAnyGround && !Motor.GroundingStatus.IsStableOnGround)
                    {
                        jumpDirection = Motor.GroundingStatus.GroundNormal;
                    }

                    // Makes the character skip ground probing/snapping on its next update. 
                    // If this line weren't here, the character would remain snapped to the ground when trying to jump. Try commenting this line out and see.
                    Motor.ForceUnground(0.1f);

                    // Add to the return velocity and reset jump state
                    currentVelocity += (jumpDirection * _jumpSpeed) - Vector3.Project(currentVelocity, Motor.CharacterUp);
                    _jumpRequested = false;
                    _jumpConsumed = true;
                    _jumpedThisFrame = true;
                }
            }

            // Reset wall jump
            _canWallJump = false;
        
        }

        private void OnDrawGizmos()
        {
            Gizmos.DrawLine(gameObject.transform.position,gameObject.transform.position+currentVelocityTemp.normalized);

        }

        /// <summary>
        /// (Called by KinematicCharacterMotor during its update cycle)
        /// This is called after the character has finished its movement update
        /// </summary>
        public void AfterCharacterUpdate(float deltaTime)
        {
            // Handle jump-related values
            {
                // Handle jumping pre-ground grace period
                if (_jumpRequested && _timeSinceJumpRequested > JumpPreGroundingGraceTime)
                {
                    _jumpRequested = false;
                }
                switch (CharaterMobility)
                {
                    case MobilityType.TrippleJump:
                        if (_mobilityRequested && _timeSinceJumpRequested > JumpPreGroundingGraceTime)
                        {
                            _mobilityRequested = false;
                        }
                        break;
                    case MobilityType.FadeJump:
                        if (_mobilityRequested && _timeSinceJumpRequested > JumpPreGroundingGraceTime)
                        {
                            _mobilityRequested = false;
                        }
                        break;
                    case MobilityType.Levitate:
                        break;
                    case MobilityType.BurstGlide:
                        
                        break;
                    case MobilityType.BlinkReverse:
                        break;
                    case MobilityType.Hook:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                

                if (AllowJumpingWhenSliding ? Motor.GroundingStatus.FoundAnyGround : Motor.GroundingStatus.IsStableOnGround)
                {
                    // If we're on a ground surface, reset jumping values
                    if (!_jumpedThisFrame)
                    {
                        
                        airJumpCounter = 0;
                        _currentMobilityStat = MobilityStat;
                        _jumpConsumed = false;
                    }
                    _timeSinceLastAbleToJump = 0f;
                }
                else
                {
                    // Keep track of time since we were last able to jump (for grace period)
                    _timeSinceLastAbleToJump += deltaTime;
                }
            }
        }

        public bool IsColliderValidForCollisions(Collider coll)
        {
            return true;
        }

        public void OnGroundHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport)
        {
        }

        public void OnMovementHit(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, ref HitStabilityReport hitStabilityReport)
        {
            // We can wall jump only if we are not stable on ground and are moving against an obstruction
            if (AllowWallJump && !Motor.GroundingStatus.IsStableOnGround && !hitStabilityReport.IsStable)
            {
                _canWallJump = true;
                _wallJumpNormal = hitNormal;
            }
        }

        public void PostGroundingUpdate(float deltaTime)
        {
        }

        public void AddVelocity(Vector3 velocity)
        {
        }

        public void ProcessHitStabilityReport(Collider hitCollider, Vector3 hitNormal, Vector3 hitPoint, Vector3 atCharacterPosition, Quaternion atCharacterRotation, ref HitStabilityReport hitStabilityReport)
        {
        }

        public void OnDiscreteCollisionDetected(Collider hitCollider)
        {
        }
        
    }
}