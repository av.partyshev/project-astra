﻿using System;
using System.Collections;
using System.Collections.Generic;
using Project3.Bullet;
using Project3.Bullet.Setup;
using Project3.Core;
using UnityEngine;
using UnityEngine.Events;

namespace Project3.Weapon.Caster
{
    

    public class CasterWeaponController:Core.Weapon
    {

        
        
        
        

        private float start;
        void Start()
        {


            switch (bulletClass)
            {
                case BulletClass.Meteora:
                    start = setup.Meteora.ParticleRange;
                    break;
                case BulletClass.Asteroid:
                    start = setup.Asteroid.ParticleRange;
                    break;
                case BulletClass.Hound:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
                
        }

        

        // Update is called once per frame
        void Update()
        {
            scrollInput = Input.GetAxis(MouseScrollInput);
            scrollState += scrollInput * 10;
            //print(scrollState);
            scrollState = Mathf.Clamp(scrollState, 0, 10);
            _generating = Input.GetMouseButton(1);
            trigger = Input.GetMouseButton(0);
            switch (bulletClass)
            {
                case BulletClass.Meteora:
                    Clip = 2;

                   
                    
                    
                    break;
                case BulletClass.Asteroid:
                    
                    Clip = MinClip + ((int) scrollState);
                    Clip = Mathf.Clamp(Clip, MinClip, MaxClip);

                    
                    // setup.Asteroid.EnergyPrice = Mathf.Lerp(setup.Asteroid.MaxEnergyPrice, setup.Asteroid.MinEnergyPrice,
                    //     (1f*Clip )/ (1f*MaxClip));
                    
                    break;
                case BulletClass.Hound:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            
            RaycastHit hit;
            
            targetPoint = target.position;
            //transform.rotation = camera.transform.rotation;








        }


        protected override IEnumerator Shoot() 
        {
            while (true)
            {
                
                
                while (!_generated)
                {
                    consumption = 0;
                    yield return null;
                }
                WaitForSeconds rate=new WaitForSeconds(60f);;
                switch (bulletClass)
                {
                    case BulletClass.Meteora:
                        rate=new WaitForSeconds(60f/setup.Meteora.RPM);
                        break;
                    case BulletClass.Asteroid:
                        rate = new WaitForSeconds((60f/setup.Asteroid.RPM));
                        break;
                    case BulletClass.Hound:
                        break;
                }

                int bulletCounter;
                int maxPerShot = Clip - (MinClip - 1);
                for (int i = 0; i<clip.Count;i++)
                {
                    
                    yield return new WaitUntil(() => trigger);
                    switch (bulletClass)
                    {
                        case BulletClass.Meteora:
                            Meteora m = clip[i].GetComponent<Meteora>();
                            
                            m.parent = transform;
                            setup.Meteora.ParticleRange = start* transform.localScale.x;
                            m.target = target;
                            m.shoot = true;
                            m.Setup(setup.Meteora);
                            
                            break;
                        case BulletClass.Asteroid:
                            if(i>=clip.Count)
                                break;
                            int j = 0;
                            for (; j < setup.Asteroid.SimultaneousShots; j++)
                            {
                                if(i+j>=clip.Count)
                                    break;
                                Asteroid a = clip[i+j].GetComponent<Asteroid>();
                                a.setup = setup.Asteroid;
                                a.parent = transform;
                                a.setup.ParticleRange = start* transform.localScale.x;
                                a.target = target;
                                a.shoot = true;  
                                Rigidbody c = clip[i+j].GetComponent<Rigidbody>();
                                c.isKinematic = false;
//                                print(i+j);
                            }
                            if(j>0)
                                i += j-1;
                            
                            // Asteroid a = clip[i].GetComponent<Asteroid>();
                            // a.setup = setup.Asteroid;
                            // a.parent = transform;
                            // a.setup.ParticleRange = start* transform.localScale.x;
                            // a.target = target;
                            // a.shoot = true;
                            break;
                        case BulletClass.Hound:
                            
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    
                    
                    Rigidbody r = clip[i].GetComponent<Rigidbody>();
                    r.isKinematic = false;
                    
                    
                    
                    //clip[i].transform.SetParent(null);
                    yield return rate;
                    //yield return null;
                }
                clip.Clear();
                UseBooked();
                _generated = false;
                yield return null;
            }
            
            
            
            
            
            
            yield break;

             
        }

        protected override IEnumerator Reload() 
        {
            
            while (true)
            {
                yield return new WaitUntil(() => (_generating && !_generated));
                foreach (var item in clip)
                {
                        Destroy(item);
                }
                clip.Clear();
                switch (bulletClass)
                {
                    case BulletClass.Meteora:
                        setup.Meteora.BloomRadius = Mathf.Lerp(setup.Meteora.MinBloomRadius, setup.Meteora.MaxBloomRadius,
                            scrollState / 10f);
                        setup.Meteora.ShotVelocity = Mathf.Lerp(setup.Meteora.MaxShotVelocity, setup.Meteora.MinShotVelocity,
                            scrollState / 10f);
                        setup.Meteora.ShotSize = Mathf.Lerp(setup.Meteora.MinShotSize,
                            setup.Meteora.MaxShotSize,
                            scrollState / 10f);
                        setup.Meteora.BloomTime = Mathf.Lerp(setup.Meteora.MinBloomTime, setup.Meteora.MaxBloomTime,
                            scrollState / 10f);
                        setup.Meteora.EnergyPrice = Mathf.Lerp(setup.Meteora.MinEnergyPrice, setup.Meteora.MaxEnergyPrice,
                            scrollState / 10f);
                        setup.Meteora.DamageScaleFactor = Mathf.Lerp(setup.Meteora.MinDamageScaleFactor, setup.Meteora.MaxDamageScaleFactor,
                            scrollState / 10f);
                        if (!Prep(setup.Meteora.EnergyPrice*Clip*Clip*Clip))
                        {
                            continue;
                        }
                        break;
                    case BulletClass.Asteroid:
                        
                        setup.Asteroid.EnergyPrice =setup.Asteroid.MaxEnergyPrice * MinClip*MinClip*MinClip/(Clip*Clip*Clip);
                        setup.Asteroid.RPM = Mathf.Lerp(setup.Asteroid.MinRPM, setup.Asteroid.MaxRPM,
                            (1f*Clip) / (1f*MaxClip));
                    
                        setup.Asteroid.SimultaneousShots =(int) Mathf.Lerp(setup.Asteroid.MinSimultaneousShots, setup.Asteroid.MaxSimultaneousShots,
                            1f*Clip/(1f* MaxClip));
                        setup.Asteroid.DamageScaleFactor = Mathf.Lerp(setup.Asteroid.MinDamageScaleFactor, setup.Asteroid.MaxDamageScaleFactor,
                            1f*Clip/(1f* MaxClip));
                        
                        if (!Prep(setup.Asteroid.EnergyPrice*Clip*Clip*Clip))
                        {
                            print(setup.Asteroid.EnergyPrice*Clip*Clip*Clip);
                            continue;
                        }
                        break;
                    case BulletClass.Hound:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                //print(_energyController.BookASegment(asteroid_energy_price, out energyIndex));
                //print(energyIndex);
                // if (!_energyController.BookASegment(asteroid_energy_price, out energyIndex))
                // {
                //     continue;
                // }

                _generated = true;
                int d = Clip;
                float c = transform.localScale.x / Clip;
                
                for (int i = 0; i < d; i++)
                {
                    // int j=0;
                    // int k=0;
                    for (int j = 0; j < d; j++)
                    {
                        for (int k = 0; k < d; k++)
                        {
                            GameObject g;
                            Rigidbody r;
                            switch (bulletClass)
                            {
                                case BulletClass.Meteora:
                                    
                                    g=Instantiate(setup.Meteora.Prefab, transform);
                                    g.transform.localScale=-c*0.05f*Vector3.one+Vector3.one*c/transform.localScale.x;
                                    g.transform.localPosition =
                                        ( (new Vector3(j, k, i)  / d) - Vector3.one * (Clip - 1)/(2*d) );
                                    Meteora m = g.GetComponent<Meteora>();
                                    r= g.GetComponent<Rigidbody>();
                           
                                    r.isKinematic = true;
                                    r.useGravity = false;
                                    clip.Add(g);
                                    break;
                                case BulletClass.Asteroid:
                                   
                                    g=Instantiate(setup.Asteroid.Prefab, transform);
                                    g.transform.localScale=-c*0.05f*Vector3.one+Vector3.one*c/transform.localScale.x;
                                    g.transform.localPosition =
                                        ( (new Vector3(j, k, i)  / d) - Vector3.one * (Clip - 1)/(2*d) );
                                    Asteroid a = g.AddComponent<Asteroid>();
                                     r = g.AddComponent<Rigidbody>();
                           
                                    r.isKinematic = true;
                                    r.useGravity = false;
                                    clip.Add(g);
                                    break;
                                case BulletClass.Hound:
                                    if (!Prep(Clip*Clip*Clip))
                                    {
                                        continue;
                                    }
                                    break;
                                default:
                                    throw new ArgumentOutOfRangeException();
                            }
                            
                            
                        }
                    
                        
                        yield return null;
                    }
                }
            }
            

             
        }

        // private void OnDrawGizmos()
        // {
        //     
        //     Gizmos.color=Color.green;
        //     Gizmos.DrawLine(transform.position,targetPoint);
        // }
    }
}
