﻿using System.Collections;
using System.Collections.Generic;
using Project3.Core;
using UnityEngine;

public class Tester : MonoBehaviour
{
    public EnergyClient ec;
    // Start is called before the first frame update
    void Start()
    {
        ec = gameObject.GetComponentInParent<EnergyClient>();
        print(ec.consumption);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
